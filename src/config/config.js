let config = {};

//DATABASE
config.db_host =  "localhost";
config.db_port =  "12345";
config.db_database =  "school";
config.db_username =  "root";
config.db_password =  "root";

//API
config.port = 3000;
config.hostname =  "localhost";

module.exports = config;
