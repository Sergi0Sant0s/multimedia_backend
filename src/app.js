const express = require('express');
const mongoose = require("mongoose");
const env = require("dotenv");
const app = express();
const users = require('./routes/users')
const classes = require('./routes/classes')
const auth = require('./routes/auth')
const jwtVerify = require('./routes/verifyToken')

//Test
app.use(express.json())
app.get('/api', (req, res) => { res.status(200).json({ "message": 'All its fine!' }) })

//routes
app.use("/api/auth", auth);
app.use("/api", jwtVerify, users);
app.use("/api", jwtVerify, classes);


env.config();
//Mongodb
mongoose.set('useCreateIndex', true);
mongoose.connect(
  process.env.DB_CONNECT, {
  useUnifiedTopology: true,
  useNewUrlParser: true
},
  () => console.log("db connected")
);

//Listen
app.listen(process.env.LISTEN_PORT, process.env.LISTEN_HOSTNAME);