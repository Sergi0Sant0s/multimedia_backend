const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  async create(request, response) {
    const usernameExists = await User.findOne({
      username: request.body.username,
    });
    if (usernameExists) return response.status(400).json({ "message": "Username already exists" });

    const hashedPw = await bcrypt.hash(request.body.password, 10);

    const user = new User({
      name: request.body.name,
      username: request.body.username,
      password: hashedPw,
    });

    try {
      const { name, username } = await user.save();
      return response.status(201).json({ name, username });
    } catch (e) {
      console.log(e);
      return response.status(400);
    }
  },

  async login(request, response) {
    const user = await User.findOne({
      username: request.body.username,
    });

    if (!user) return response.status(400).send("Username is not found");

    const validPW = await bcrypt.compare(request.body.password, user.password);

    if (!validPW) return response.status(400).send("Invalid Password");

    const exp = Math.floor(Date.now() / 1000) + (60 * 60);
    const token = jwt.sign({
      exp,
      data: { _id: user._id }
    }, process.env.TOKEN);
    response.header("auth-token", token).json({ exp, token });
  },

  async logout(request, response) {
    response.status(200).json({ name, username });
  }
}