
const express = require("express");
const router = express.Router();

const ClassController = require('../controllers/ClassController')

router.get('/class', ClassController.index)
router.post('/class', ClassController.create)
router.put('/class/:id', ClassController.edit)
router.delete('/class/:id', ClassController.delete)

module.exports = router;